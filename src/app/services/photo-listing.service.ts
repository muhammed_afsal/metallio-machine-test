import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PhotoListingService {

  constructor(private http: Http) { }


  getPhotos(): any {
    let url = "https://jsonplaceholder.typicode.com/photos";
    return this.http.get(url).pipe(map(resp => resp.json()))
  }
}
