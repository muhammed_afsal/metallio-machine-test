import { DataSource } from '@angular/cdk/table';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';
import { PhotoListingService } from 'src/app/services/photo-listing.service';

@Component({
  selector: 'app-photo-listing',
  templateUrl: './photo-listing.component.html',
  styleUrls: ['./photo-listing.component.scss']
})

export class PhotoListingComponent implements OnInit {

  photo_list:any=[]
  compared_photo_list:any=[]
  displayedColumns: string[] = ['name', 'id', 'url','thumb', 'title'];
  dataSource
  constructor(public photoService:PhotoListingService) { }

  ngOnInit() {

    this.dataSource = new PhotoDataSource()
    this.dataSource.use(this.compared_photo_list.slice())

    this.photoService.getPhotos().subscribe(result => {
      this.photo_list = result
    });
  }

  compare(photo){
    photo.is_compare =true
    this.compared_photo_list.push(photo)
    this.dataSource.use(this.compared_photo_list.slice())
  }

  remove(photo,index){
    photo.is_compare =false
    var index = this.compared_photo_list.indexOf(photo);
    this.compared_photo_list.splice(index, 1);
    this.dataSource.use(this.compared_photo_list.slice())
  }

}


export class PhotoDataSource extends DataSource<any> {

  dataWithTotal = new BehaviorSubject<any>([]);

  use(selecteditems) {
    this.dataWithTotal.next([...selecteditems]);
  }

  connect(): Observable<any> {
    return this.dataWithTotal.asObservable();
  }

  disconnect() { }
}

